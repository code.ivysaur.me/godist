# godist

![](https://img.shields.io/badge/written%20in-bash-blue)

A template shell script for building golang binaries for distribution.

There are a number of nonobvious steps involved when building release archives for a golang binary. This template build script is intended to be copied into your own repository as a base.

- Build for mulitple platforms
- Archive in platform native format
- Always rebuild dependencies
- Strip sensitive symbol information (`ldflags '-s -w'`)
- Strip sensitive path information (remove baked `$GOPATH`)
- Strip sensitive user information (`tar --owner=0 --group=0`)
- ISC license
- "Works on my machine" (go 1.7 win64 with cygwin)

## Changelog

2017-02-11 1.2
- Fix an issue with `trimpath` if the `$GOPATH` contained a trailing slash
- [⬇️ godist-1.2.zip](dist-archive/godist-1.2.zip) *(1.76 KiB)*


2016-10-08 1.1
- Use `trimpath` instead of custom stripper for removing baked `$GOPATH`
- [⬇️ godist-1.1.zip](dist-archive/godist-1.1.zip) *(1.71 KiB)*


2016-05-01 1.0
- Initial public release
- [⬇️ godist-1.0.zip](dist-archive/godist-1.0.zip) *(1.89 KiB)*

